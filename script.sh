#!/bin/bash

docker network list | grep rpi-net

if [ $? != 0 ]
then
    docker network create rpi-net
fi

docker rm -f $(docker ps -aq)

docker run -d -p 8002:8002 --net rpi-net --privileged --name rpi-io angellazaro/ticbox-rpi-io
docker run -d --name watchtower -v /var/run/docker.sock:/var/run/docker.sock v2tec/watchtower:armhf-latest --interval 30
docker run -d --net rpi-net --name blinker angellazaro/ticbox-blinker